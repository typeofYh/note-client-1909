const baseRouter = [
  {
    path: "/",
    component: () => import("views/article/index.vue"),
    meta: {
      title: "文章",
      nav: true,
    },
  },
  {
    path: "/archives",
    component: () => import("views/archives/index.vue"),
    meta: {
      title: "归档",
      nav: true,
    },
  },
  {
    path: "/knowledge",
    component: () => import("views/knowledge/index.vue"),
    meta: {
      title: "知识小册",
      nav: true,
    },
  },
  {
    path: "/page/:id",
    component: () => import("views/page/index.vue"),
    meta: {
      nav: false,
    },
  },
]

export default baseRouter
