import { defineStore } from "pinia"
import { base } from "@/services"

const useStore = defineStore("baseStore", {
  state: () => ({
    navList: [],
  }),
  actions: {
    async getNavData() {
      const { data } = await base.getNavPage()
      this.$patch({
        navList: data[0].filter((item: any) => item.id),
      })
    },
  },
})

console.log(useStore)
export default useStore
