const statusCodeMap: any = {
  "400": "您输入的参数有问题~",
  "401": "您当前身份不明确，或者登录态过期，请重新登录~",
  "403": "您暂无权限~",
  "500": "服务器异常，请联系工作人员~",
}

export default statusCodeMap
